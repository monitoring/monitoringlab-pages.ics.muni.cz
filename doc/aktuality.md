# Aktuality

Vážení uživatelé,

do tohoto prostoru Vám budeme vkládat informace týkající se novinek, změn a úprav v novém monitoringu a nahradíme tak e-mailovou komunikaci.

### 20.7.2022
Aktualizace webu na [v2.11](https://github.com/Icinga/icingaweb2/releases/tag/v2.11.0) a jádra na [v2.13.4](https://github.com/Icinga/icinga2/releases/tag/v2.13.4)

[https://gitlab.ics.muni.cz/monitoring/icinga/-/releases/v2.13.4](https://gitlab.ics.muni.cz/monitoring/icinga/-/releases/v2.13.4)

[https://gitlab.ics.muni.cz/monitoring/icingaweb/-/releases/v2.11.1](https://gitlab.ics.muni.cz/monitoring/icingaweb/-/releases/v2.11.1)

### 22.6.2022
Aktualizace webu na [v2.10.1](https://icinga.com/blog/2022/03/23/releasing-icinga-web-v2-10/)

[https://gitlab.ics.muni.cz/monitoring/icingaweb/-/releases/v2.10.1](https://gitlab.ics.muni.cz/monitoring/icingaweb/-/releases/v2.10.1)

### 13.6.2022

Bezpečnostní aktualizace [v2.12.3](https://icinga.com/blog/2022/04/14/releasing-icinga-2-13-3-and-2-12-7/).

[https://gitlab.ics.muni.cz/monitoring/icinga/-/releases/v2.13.3](https://gitlab.ics.muni.cz/monitoring/icinga/-/releases/v2.13.3)

### 26.10.2021

Dne 16.11.2021 mezi 8.00h a 9.00h proběhne **odstávka** a větší údržba monitoringu:

 * Icinga bude aktulizována na [v2.13.1](https://icinga.com/blog/2021/08/03/releasing-icinga-2-13-0/). Icingaweb na [v2.9.2](https://icinga.com/blog/2021/07/12/releasing-icinga-web-v2-9-0/).
 * Docker kontejner bude aktualizován na Debian 11 (z Debian 9) a s ním i nástroje jako NRPE
 * Databáze bude přesunuta do HA clusteru
 * Separace monitorovaního jádra a webu a další změny "pod kapotou"

Aktualizace bude mít vliv i na OpenSSL knihovny, takže starší klienti mohou přestat fungovat.

Týká se to primárně Windowsového NSClient++, který má problém z důvodu krátké délky Diffie-Hellamn klíče. Pro takové klienty je třeba klíče vygenerovat nové.  
To lze udělat například pomocí `openssl` příkazem:
```console
openssl dhparam -out nrpe_dh_2048.pem 2048
```
a následně aktualizovat klíč v `nsclient.ini`. Nebo použít pro kontrolu [WMI](../definice-objektu-kontrol#check_wmi)

### 4.3.2021

Bezpečnostní update v2.12.3. Update linuxového jádra serveru.

### 8.10.2020

Icinga byla aktualizována na [v2.12](https://icinga.com/2020/08/05/releasing-icinga-2-12-2-11-5-icinga-db-connection-security-dsl/).

### 28.8.2020

Proběhla bezpečností aktualizace webu Icinga2 na [v2.8.2](https://icinga.com/2020/08/19/icinga-web-security-release-v2-6-4-v2-7-4-and-v2-8-2/).  

A přidána možnost vizualizovat objekty na celé [Zemi](https://monitor.ics.muni.cz/icingaweb2/globe) :-)

### 15.7.2020

[Icinga2](https://monitor.ics.muni.cz), její [Wiki](https://monitor.ics.muni.cz/wiki) a [Grafana](https://grafana.ics.muni.cz) byly napojeny na Jednotné přihlášení MU (speciální díky Pavlovi Břouškovi za pomoc).

Při té přiležistosti byla provedena generálka a některé elementy (primárné grafy a prvky webového rozhraní) optimalizovány a měly by být o něco responsivnější.

Pokud objevíte nějaké anomálie nebo Vám něco nefunguje pište na [icinga-adm](mailto:icinga-adm@ics.muni.cz).

### 3.7.2020

Přepsal jsem logiku zobrazující grafy. Grafy se nyní vytvářejí pro majoritu kontrol mnohem rychleji a automaticky - viz. [Grafy](../vytvoreni-hosta)  
Také bych upozornil na instanci [Grafany](https://grafana.ics.muni.cz) kde si můžete vytvářet grafy dle vlastních preferencí a je možné je pak namapovat na hosta/službu dle přání.

Byl opraven nepříjemný bug z února ohledně mizejících služeb při použití `PUT` na již existující objekt.

Icinga byla aktualizována na verzi `2.11.4`.

Závěrem bych upozornil že `IcingaDB` - nová froma ukládáni dat spolu s Redis databází, která řeší neskutečně otravnou prodlevu kdy se nově nahrané objekty neobjevují na webu je ve stavu RC1. Její vydání jěště nějaký čas potrvá, ale jakmie bude vydána budu nasazovat ASAP.

### 21.5.2020

Přídán konektor pro MS Teams.

### 5.3.2020

Přídán plugin pro monitoring síťových komponentů a aktualizace Icingy  
[check_nwc_health](https://github.com/lausser/check_nwc_health) je rozšíření pro kontrolu stavu síťvých prvků.  
Základní popis v [Icinga ITL](https://icinga.com/docs/icinga2/latest/doc/10-icinga-template-library/#nwc_health)

Icinga byla aktualizována na verzi 2.11.3.

### 17.2.2020

Použití `cascade=1` je znovu povoleno.

V Icinze se při použítí API nachází [bug](https://github.com/Icinga/icinga2/issues/7726). Při zapisování objektů pomocí API je potřeba vždy použít adekvátní HTTP metodu:
 - pokud objekt ne-existuje, použít `PUT`
 - pokud existuje metodu `POST`
 - pro znovu-vytvoření objektu je pořeba objekt nejdříve smazat a následně použít `PUT` pro jeho znovu vytvoření

Bug je nahlášen upstreamu, bohužel se zdá, že řešení nebude brzké. Pokud tedy používáte vlastní nástroje na API, mějte na paměti nejdříve zjisit zda-li objekt existuje a podle toho použít adekvátní metodu.

### 27.1.2020

Přidán [check_git](../definice-objektu-kontrol) plugin pro kontrolu dostupnosti gitových pozitářů.

### 20.1.2020

Přídán wmic klient a plugin [check_wmi](../definice-objektu-kontrol) umožnující provádět kontroly strojů s OS Windows pomocí Windows Management Instrumentation.

### 14.1.2020

Dříve používaný monitorovací nástroj NAGIOSbude pro možnost dokončení migrace zbývajících monitorovaných zařízení/služeb do nového nástroje ICINGA jednotlivými týmy univerzity v provozu max. do 31. 3. 2020.

### 1.11.2019

Přidán repositář pro PowerShell. Děkujeme Adriánu Rošinci za sdílení.

### 24.10.2019

Bohužel budeme muset opět požádat o omezení užití mazání objektů za použití 'cascade=1'. Chyba kde Icinga nesmaže všechny závislé objekty se nadále projevuje v cca 10% případů.

To značí že pro smazání například hosta je třeba nejdříve smazat jeho notifikace, pak služby a následně až samotného hosta.

V případě že je toto příliš problematické a je pro Vás jednodušší použít mazáni za pomocí cascade napište prosím před samotným výmazem na icinga-adm ať můžeme provést kontorlu zda vše v pořádku proběhlo.

### 27.9.2019

Je možné už zase začít používat mazání za pomocí cascade=1.

### 24.9. 2019

Dnes byla Icinga aktualizována na verzi [r2.11.0-1](https://icinga.com/2019/09/19/icinga-2-11/). Většina změn je spíše typu "pod kapotou" vylepšujících funkce API,
zabezpečení, nové příkazy pro ITL a další. Zájemci mohou kouknout na [changelog](https://github.com/Icinga/icinga2/blob/master/CHANGELOG.md).

### 12.9.2019

#### Mattermost plugin

Na icinga-demo byl přídán [Mattermost plugin](https://github.com/Reamer/icinga2-mattermost) pro posílání notifikací do Mattermost chatu.
Po otestovnání bude zaveden také na ostrou versi.

#### PHP API client

Přídán odkaz na repositář php klienta pro API. Poděkování Pavlu Břouškovi za sdílení.

### 09.08.2019
Vyřešili jsme požadavek [RT-488766](https://rt.cesnet.cz/rt/Ticket/Display.html?id=488766&results=c8e8aa9fa9097a6f15eeb84bc5358766) a zprovoznili modul [Dependencies](https://monitor.ics.muni.cz/icingaweb2/dependency_plugin/module/network) v GUI Icingy,
kteý by měl primárně sloužit pro zahlídaní síťových prvků, ale třeba budete jeho možnosti chtít vyzkoušet a použít také.
Informace, které se mohou hodit naleznete [ZDE](https://github.com/visgence/icinga2-dependency-module)

### 30.7.2019

V jádře Icingy jsme objevili poměrně nepříjemnou chybu která při mazání objektu s použitím "cascade=1" nesmaže na objekt navázané notifikace. Výsledkem je
inkonsistentní konfigurace, která může mít za následek různé anomálie od absence hostů po případný pád samotné Icingy.

Chyba byla nahlášena vývojářům. Dokud nebude opravena prosím nepoužívejte "cascade=1".


Ref.: [https://github.com/Icinga/icinga2/issues/7330](https://github.com/Icinga/icinga2/issues/7330)

### 18.07.2019 - 28.07.2019
#### Delší reakční doba
Vážení uživatelé z důvodu čerpání dovolených v termínu od 18.07.2019 - 28.07.2019 se může stát, že nastane prodleva při vyřizování vašich požadavků.
Děkujeme za pochopení

### 12.07.2019
#### Znovunasazení hostů 
Po včerejším nešťastném smazání všech hostů se nám podařilo znovu zprovoznit Icingu2 a lépe ošetřit její zálohování, nicméně apelujeme na Vás, abyste si i nadále udržovali lokální konfigurace pro 
případné nešťastné náhody. 
Znovu připomínáme, že všichni uživatelé API mají administrátorská prává (Icinga2 bohužel v současné chvíli neumožňuje rozumné použití uživatelkých rolí), a proto 
je velmi důležité provádět změny na API uvážlivě a opatrně, viz [obecné zásady použití](../../01_pravidla_pouziti)

### 09.07.2019
#### Oprava zobrazení grafů
Oprava[RT 479759](https://rt.muni.cz/rt/Ticket/Display.html?id=479759) - u některých strojů se neplnila databáze a tudíž nebyly vytvářeny grafy.
### 28.06.2019
#### Zřízení nové RT fronty
- název fronty: uvt-icinga-adm
- alias: icinga-adm@ics.muni.cz

### 27.06.2019
#### Update dokumentace 
- přidali jsme položku [Zahlídání strojů dohledovým centrem](../../07_zahlidani_pres_dohledove_centrum)

### 26.06.2019
#### Update uživatelských práv
- matice práv TBD

### 01.06.2019
#### ÚVODEM

dnešním dnem počínaje začal tým systémové správy DITI provozovat nový monitorovací nástroj Icinga2. Produkční prostředí je plne připraveno, včetně uživatelské
dokumentace ([1]https://monitoring.gitlab-pages.ics.muni.cz/dokumentace/) a nahradí tak stávající nástroj Nagios, jehož podporu ukončíme s koncem roku 2019.

Protože někteří kolegové z řad CIT se už jednotlivě ozvali, rozhodli jsme se nabídnout možnost využití Icingy2 i ostatním kolegům z MU. Mnozí ze stávajících uživatelů nástroje Nagios požadovali
možnost vlastní správy/konfigurace přes API pro rychlejší a pohodlnější správu vlastních strojů a služeb. Návod jak na to a další rady a postupy naleznete právě ve výše uvedené dokumentaci, což znamená, že od nás budete mít vše potřebné pro zahájení migrace vašich strojů a služeb. Zároveň doporučujeme, aby jste s migrací vašich strojů neotáleli, ať v co
nejkratší době zjistíme a budeme schopni odladit případné překážky a problémy. 

#### POZOR!!!

Pro přístup do weboveho rozhraní je potřeba dodat seznam uživatelů a oprávnění, které mají v dané skupině mít. Přístup do API je na vyžádání, defaultně není ve webovém rozhraní
nic vidět, je třeba požádat o přístupy [2]**icinga-adm@ics.muni.cz**, alias je směrován do RT fronty **[3][uvt-icinga-adm](https://rt.cesnet.cz/rt/Search/Results.html?Query=Queue%20%3D%20%27uvt-icinga-adm%27%20AND%20(Status%20%3D%20%27new%27%20OR%20Status%20%3D%20%27open%27%20OR%20Status%20%3D%20%27stalled%27%20OR%20Status%20%3D%20%27feedback%27))**.

### Instance:
Produkční: [4] https://monitor.ics.muni.cz/icingaweb2/
Demo: [5] https://icinga-demo.ics.muni.cz/icingaweb2/

- přístupové údaje: UČO/sekundární heslo

#### Dovolte mi ještě jednou připomenout výhody Icingy2 oproti stávajícímu Nagiosu s ohledem na požadavky drtivé většiny uživatelů:
- veřejně přístupný Dashboard s přehledem zahlídaných strojů a služeb
- možnost exportu/importu dat prostřednictvím Rest API,
- graficka mapa síťových prvků včetně závislostí
- automatizovaná správa (přidávání/odebírání strojů)
- máme připraveno řešení v podobě puppetu a ansible, ktere můžete použít,
- generování statistik a grafů (možnost připojení Grafany a ELKu, prostřednictvím InfluxDB)
- možnost nasazení custom sond,
- možnost autentizace prostřednictvím UČO + sekundární heslo (pro kolegy z CESNETu
- bude možno se připojit i přes federované přihlášení),
- alerting (mail, SMS, SNMP trap, atd.),
- multitenance (více úrovní oprávnění, možnost vytvářet různé pohledy pro uživatele, atd.),
- vizualizace veličin sledovaných entit a další,

#### Správa ve vlastní režii
Tento režim správy je určen každému, kdo si chce sám spravovat své stroje a služby.
Konfigurace se provádí přes [6] [API Icingy](https://icinga.com/docs/icinga2/latest/doc/12-icinga2-api/). Jak budou stroje a služby
monitorovány, je po aplikaci níže uvedených a
obecných pravidel zcela v režii správců monitorovaných strojů a služeb.
Toto je doporučovaná forma použití monitoringu.
- monitorovat je možné pouze vlastní stroje 
- při potřebě informací o stavu stroje/služby, kterou provozuje někdo jiný, je třeba domluvit se na zahlídání (pak je mozno videt stav ciziho stroje v GUI Icingy - nelze jakkoliv manipulovat)
- při vytváření skupin v názvu použiji vhodný prefix, kvůli předcházení kolizí ve jménech (např. cloud, cerit, diti, csirt, win..)
- mějte na paměti, že stále můžete použít `display_name`, kde si můžete zvolit vhodný název,
- přes API je možné definovat i konfigurační soubory, je možné tedy využít potenciálu Icingy, avšak je třeba používat s rozvahou

#### Obecné zásady použití
Toto jsou zásady pro monitoring, které jsou platné pro všechny. Otevřením možnosti konfigurace je třeba nastavit pravidla, aby konfigurace monitoringu byla udržitelná i do budoucna.
- Změny v konfiguraci je třeba provádět uvážlivě
- Každý stroj bude zaveden pod jménem odpovídajícím jeho FQDN a z popisu bude zřejmé, kdo je za něj zodpovědný
- stroje bez DNS záznamu budou pojmenovány dle IP
- U stroje je důrazně doporučené definovat rodiče - například router, ke kterému je stroj připojený
- Každý stroj je možné monitorovat pouze jednou, pokud má více skupin zájem na získávání notifikací o stavu, domluví se se správcem služby/stroje.
- Stroje/služby/kontakty se mohou dávat pouze do vlastních skupin a do skupin globálních, pokud je potřeba vazba na cizí skupinu, je třeba domluvit se se správcem.
- Globální skupiny mají v názvu *global*
- Členství ve skupině se definuje u stroje/služby/kontaktu, ne ve skupině samotné, změny v monitoringu jsou potom jednodušší, nevzniká překryv v konfiguraci,

#### Prosím veškeré dotazy směřujte do uvedené RT fronty - uvt-icinga-adm, alias: icinga-adm@ics.muni.cz


Links:
1. https://monitoring.gitlab-pages.ics.muni.cz/dokumentace/
2. mailto:icinga-adm@ics.muni.cz
3. https://monitor.ics.muni.cz/icingaweb2/
4. https://icinga-demo.ics.muni.cz/icingaweb2/
5. https://icinga.com/docs/icinga2/latest/doc/12-icinga2-api/
6. https://monitor.ics.muni.cz
