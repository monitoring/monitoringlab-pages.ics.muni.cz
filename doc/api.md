# API

## Co bude potřeba

1. API účet pro Icingu
    * Poslat (nejlépe digitálně podepsaný e-mail) na icinga-adm@ics.muni.cz
1. vyšší než defaultní oprávnění do webového rozhraní
    * Poslat seznam uživatelů/skupinu na icinga-adm@ics.muni.cz
    * Přihlášení je vázáno na Jednotné přihlášení UČO/primární heslo
1. Nástroj pro práci s JSON REST API
    * použít můžeme například `curl` nebo powershell či ansible
1. servery a služby, co chci monitorovat
1. do jakých skupin chci servery a služby zařadit
1. kdo bude dostávat upozornění na změny stavu serveru
1. povolit na firewallu přístup z monitoringu
    * adresy:`147.251.7.8/29` a `2001:718:801:407:13::/80`, NRPE port 5666.
    * V případě použití [Icinga agenta](https://icinga.com/docs/icinga2/latest/doc/07-agent-based-monitoring/#icinga-agent) port 5665.

## Zahlídnání stroje nebo služby

### Než začneme

Dokumentace Icingy je k dispozici na adrese https://icinga.com/docs/icinga2/latest/, je dobré mít alespoň základní přehled o kapitolách [API](https://icinga.com/docs/icinga2/latest/doc/12-icinga2-api/), [Object Types](https://icinga.com/docs/icinga2/latest/doc/09-object-types/) a [ITL](https://icinga.com/docs/icinga2/latest/doc/10-icinga-template-library/).

Dále je důležité seznámit se s [pravidly použití](../pravidla).

Příklady zde uvedené budou používat volání API pomocí CLI nástroje `curl`. Pokud vám tento nástroj nevyhovuje, stejného výsledku dosáhnete jakýmkoliv nástrojem.

### Příklady

**Ansible**:
```yaml
- uri:
    url: "http://host/api"
    user: "username"
    password: "password"
    method: POST
    return_content: yes
    headers:
      Accept: "application/json"
      Content-Type: "application/json"
    body_format: "json"
    body: {"configure":".*", "write":".*", "read":".*"}
```

**PowerShell**:
```powershell
$url = "https://monitor.ics.muni.cz:5665/v1"
$secpasswd = ConvertTo-SecureString "zde_je_heslo" -AsPlainText -Force
$mycreds = New-Object System.Management.Automation.PSCredential ("uzivatel_API", $secpasswd)
$stuff = Invoke-RestMethod -Uri $url -Method Get -Credential $mycreds

```

**cURL**:
```shell
$ curl -i -u username:password -H "content-type:application/json" -XPOST http://host/api -d '{"configure":".*","write":".*","read":".*"}'
```

Všechny ukázané způsoby jsou ekvivalentní a je jen na vás, který použijete, příklady pro další aplikace jsou vítány. Dále budu v příkladech ukazovat pouze volané `URI` a posílaná `DATA`. Jako `username` a `password` použijete to, co máte přidělené pro API.

Metody jsou čtyři:

| Metoda: | Použití |
|---------|---------|
| GET     | Pro získání informací o objektech, volání je read-only, nezmění tedy žádný objekt |
| POST    | Modifikuje existující objekt |
| PUT     | Vytvoří nový objekt, PUT request musí obsahovat všechny povinné atributy objektu |
| DELETE  | Odstraní objekt vytvořený přes API, neobsahuje kontrolu existence objektu |


## Omezení API

 - Přes API nelze vytvářet úplně vše, co konfigurace Icingy umožňuje. Omezení jsou v případě [`assign where ...`](https://icinga.com/docs/icinga2/latest/doc/03-monitoring-basics/#group-assign-intro) u skupin a [`apply rules`](https://icinga.com/docs/icinga2/latest/doc/15-troubleshooting/#configuration-troubleshooting). Více viz. [Icinga2 bug tracker](https://github.com/Icinga/icinga2/issues/4084)

 - Konfiguraci je samozřejmě možné vytvořit jako soubory, tuto možnost ale nepodporujeme. Nicméně je možné toho dosáhnout použitím [satelitu](../pravidla#satelit-napojeny-na-hlavni-instanci).

 - Dále, pokud změníte šablonu, která již byla použita v jiném objektu, tato změna se neprojeví. Je potřeba daný objekt zrušit a znovu vytvořit.

 - U objektů typu Host, Service, User a ekvivalentních *není možné* měnit později atribut `groups`. Zde platí to stejné jako u šablon.

---

**POZOR**

Při pojmenování objektů používejte vždy malá písmena. Názvy jako `check_SSH` a `check_ssh` mohou být za jistých okolností stejné i různé!

---


## URL API

API poslouchá na portu 5665 na stroji monitoring.ics.muni.cz. Konkrétní adresa použitá pro volání API pak může vypadat následovně:

```
https://monitor.ics.muni.cz:5665/v1/objects/hosts/monitor.ics.muni.cz
```

Kde `hosts` je typ objektů, které nás zajímají (například: hosts, services, hostgroups, notifications...) a monitor.ics.muni.cz je jméno daného objektu. Takže například při zavolání uvedené adresy metodou GET získáme veškeré informace o daném objektu.
