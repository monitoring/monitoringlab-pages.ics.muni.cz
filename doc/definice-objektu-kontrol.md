# Definice objektů kontrol

## Oficiální rozšíření

Ústavní Icinga používá oficiálně podporované kontroly které jsou součástí samotné Icingy, projektu Monitoring plugins a Nagios plugins.

Definici samotných objektů a jejich vlastností taktéž v [oficiální dokumentaci](https://icinga.com/docs/icinga2/latest/doc/09-object-types/)

## Neoficiální rozšíření

### Business Process

Vizualizace a monitorování hierarchických procesů.

Přístup k modulu má každý účet s navýšenými právy. Práva na vytváření a editaci samotného procesu jsou vázána na prefix (např.: diti-ansible).
Prefix je standardně jméno uživatele API nebo oddělení. Může být ale na žádost změněno na něcé jiného.

[Dokumentace](https://github.com/Icinga/icingaweb2-module-businessprocess)

### Vizualiazce závislostí

'Dependency-module' pro mapovaní síťové topologie a stavu v reálném čase. Visualizuje vazby mezi objekty.

[Dokumentace](https://github.com/visgence/icinga2-dependency-module)

### Mattermost connector

[Rožšíření](https://github.com/Reamer/icinga2-mattermost) posílající notifikace do zvoleného kanálu komunikačního nástroje Mattermost.

Pro použití je třeba u uživatele nastavit vlastní proměnné `url` s adresou Mattermost webhooku a `channel` se jménem kanálu (jméno nesmí obsahovat mezery a speciální znaky).
Samotné notifikace se volají:  
`mattermost_host` pro notifikace hosta  
`mattermost_service` pro notifikace služby

Uživatel:  
```json
{   "attrs": {
    "display_name": "Franta Vopršálek",
    "email": "franta@voprsalek.cz",
    "pager": "+420132456789"
    "vars": {
      "url": "https://mattermost-url.cz/hooks/5ebcada0c7f643ff93729",
      "channel": "monitoring",
      "oneline": "false"
    }
  }
}

```

Notifikace hosta:  
```json
{   "attrs": {
    "command": "mattermost_host",
    "host_name": "domena.cz",
    "users": [ "UCO@muni.cz" ]
  }
}

```

Notifikace služby:  
```json
{   "attrs": {
    "command": "mattermost_service",
    "host_name": "domena.cz",
    "service_name": "check_http",
    "users": [ "UCO@muni.cz" ]
  }
}
```

### MS Teams connector

[Rožšíření](https://github.com/NeverUsedID/icinga2-msteams) posílající notifikace do zvoleného kanálu komunikačního nástroje Microsoft Teams.

Pro použití stačí u uživatele nastavit vlastní proměnnou `vars.teams` jejíž obsah bude adresa webhooku vytvořeného v MS Teams a zvolení typu samotné notifikace:  
`teams-host-notification` pro notifikace hosta  
`teams-service-notification` pro notifikace služby

Uživatel:  
```json
{   "attrs": {
    "display_name": "Franta Vopršálek",
    "email": "franta@voprsalek.cz",
    "pager": "+420132456789"
    "vars": {
      "teams": "https://outlook.office.com/webhook/4b289fae-e8f2-4395-8d46-238c6f47b3a0@11904f23-f0db-4cdc-96f7-390bd55fcee8/IncomingWebhook/5ebcada0c7f643ff93729"
    }
  }
}

```

Notifikace hosta:  
```json
{   "attrs": {
    "command": "teams-host-notification",
    "host_name": "domena.cz",
    "users": [ "UCO@muni.cz" ]
  }
}

```

Notifikace služby:  
```json
{   "attrs": {
    "command": "teams-service-notification",
    "host_name": "domena.cz",
    "service_name": "check_http",
    "users": [ "UCO@muni.cz" ]
  }
}
```


## Oficiální kontroly

Definice oficiálních kontrol najdete v [Icinga Template Library](https://icinga.com/docs/icinga2/latest/doc/10-icinga-template-library/).

## Neoficiální kontroly

Kromě standardních kontrol jsou navíc součástí Icingy ještě tyto kontroly:

### check_openvpn

[Dokumentace](https://github.com/liquidat/nagios-icinga-openvpn)

```json
{   "attrs": {
    "check_command": "check_openvpn",
    "vars": {
      "tcp": "true",
      "port": "443",
      "openvpn_address": "mojevpn.cz"
    }
  }
}

```

### check_json

[GitHub](https://github.com/drewkerrigan/nagios-http-json)  
[Dokumentace](https://exchange.nagios.org/directory/Plugins/Websites%2C-Forms-and-Transactions/http-2Djson--2D-Rule-based-JSON-parser-via-http%28s%29/details)

Jméno kontrolního příkazu bylo v rámci zachování konsistence změněno na `check_json`.

Příklad použití:  
JSON výstup ze kterého se snažíme monitorovat stav nějakého klíče:

```json
{
   "dups-a-plc.m2" : {
      "current_attempt" : "1",
      "current_problem_id" : "0",
      "current_state" : "0",
      "host_name" : "dups-a-plc.m2",
      "is_flapping" : "0",
      "last_state_change" : "1582549373",
      "max_attempts" : "2",
      "plugin_output" : "PING OK - Packet loss = 0%, RTA = 0.55 ms",
      "problem_has_been_acknowledged" : "0",
      "scheduled_downtime_depth" : "0",
      "state_type" : "1"
   }
```

ATRIBUTY pro Icingu kde budeme monitorvat stav klíče `current_state`:

```json
        {
            "attrs": {
                "check_command": "check_json",
                "check_interval": 100.0,
                "vars": {
                    "address": "147.251.48.5",
                    "http_json_field_separator": ";",
                    "http_json_key_equals": "dups-a-plc.m2/DUPS A stav;current_state,0",
                    "http_json_key_exists": "dups-a-plc.m2/DUPS A stav;current_state",
                    "http_json_path": "/noauth/nagios.mpl?view=sal_cerit;format=json",
                    "http_json_ssl": "true"
                }
            }
        }
```

### check_wmi

[Modul](https://edcint.co.nz/checkwmiplus/) pro kontrolu OS Windows za použití Windows Management Instrumentation

Nastavitelné proměnné:

| Proměnná: | Vlastnost: |
| ------ | ------ |
| `wmi_user` | Doména/Uživatel - doména je nepovinný údaj |
| `wmi_password` | Heslo uživatele |
| `wmi_mode` | [Režim kontroly](https://github.com/speartail/checkwmiplus/blob/master/check_wmi_plus.README.txt#L254)
| `wmi_submode` | Specifikace submódu u některýh kontrol |
| `wmi_arg1` | Argument číslo jedna. Jeho význam závisí na režimu kontroly |
| `wmi_arg2` | Argument číslo dvě. Jeho význam závisí na režimu kontroly |
| `wmi_arg3` | Argument číslo tři. Jeho význam závisí na režimu kontroly |
| `wmi_arg4` | Argument číslo čtyři. Jeho význam závisí na režimu kontroly |
| `wmi_arg5` | Argument číslo pět. Jeho význam závisí na režimu kontroly |
| `wmi_delay` | Prodleva mezi 2 konsekutivními dotazy které proběhnou během jednoho volání pluginu |
| `wmi_warn` | Varování |
| `wmi_crit` | Kritické |
| `wmi_nodatamode` | Nevracej 'Unknown error' pokud plugin nevratí žádná data |
| `wmi_nokeepstate` | Vypne defaultní režim udžování stavů mezi jednotlivými běhy pluginu |


```json
{   "attrs": {
    "check_command": "check_wmi",
    "vars": {
      "wmi_user": "ICS/WMIu",
      "wmi_password": "supr_sikrit_pass",
      "wmi_mode": "checkcpu"
    }
  }
}

```

[**Kompletní dokumentace**](https://edcint.co.nz/checkwmiplus/category/documentation/)

[**README** vysvětlující různé přepínače a režimy](https://github.com/speartail/checkwmiplus/blob/master/check_wmi_plus.README.txt)

### check_git

[Dokumentace](https://github.com/rfay/check_git)

`check_git_url` - adresa repozitáře

`check_git_push` - Zkontroluj zda-li je možné nahrát do repozitáře. Vyžaduje SSH klíče.

`check_git_ssh_keyfile` - Název SSH klíče. Klíš musí být na straně Icingy. Pro využítí této kontroly s klíčem kontaktujte [icinga-adm@ics.muni.cz](mailto:icinga-adm@ics.muni.cz)


```json
{   "attrs": {
    "check_command": "check_git",
    "vars": {
      "check_git_url": "urlkegitu.cz/mojerepo.git"
    }
  }
}

```

### check_nwc_health

Rozšíření používající SNMP pro kontrolu síťových prvků (statistiky rozhrani, kontrola stavu hardwaru, HSRP, etc.)

Příkazy pro plugin se nachází v [Icinga Template Library](https://icinga.com/docs/icinga2/latest/doc/10-icinga-template-library/#nwc_health)

[List podporovného hardwaru](https://labs.consol.de/nagios/check_nwc_health/index.html)

```json
{   "attrs": {
    "check_command": "nwc_health",
    "vars": {
      "nwc_health_mode": "hardware-health"
    }
  }
}

```

### radius_check

[Jednoduchý plugin](https://github.com/SimoneLazzaris/radius_check) pro kontrolu authentifikace oproti RADIUS.

```json
{   "attrs": {
    "check_command": "radius_check",
    "vars": {
      "radius_check_username": "uzivatel@realm.tld",
      "radius_check_password": "UserPass",
      "radius_check_secret": "tajemstvi"
    }
  }
}

```

Nastavitelné proměnné:

| Proměnná: | Vlastnost: |
| ------ | ------ |
| `radius_check_address` | Adresa serveru. Bez zadání bude použita adresa hosta kterému služba patří. |
| `radius_check_port` | Port serveru. Výchozí hodnota 1812. |
| `radius_check_username` | Uživatel. |
| `radius_check_password` | Heslo uživatele. |
| `radius_check_secret` | Tajemství. |

### rad_eap_test

[CESNETí](https://github.com/CESNET/rad_eap_test) wrapper pro [eapol_test](https://w1.fi/wpa_supplicant/devel/testing_tools.html) pro testování RADIUS serverů.

```json
{   "attrs": {
    "check_command": "rad_eap_test",
    "vars": {
      "rad_eap_test_user": "uzivatel@realm.tld",
      "rad_eap_test_pass": "UserPass",
      "rad_eap_test_secret": "tajemstvi"
    }
  }
}

```

Nastavitelné proměnné:

| Proměnná: | Vlastnost: |
| ------ | ------ |
| `rad_eap_test_address` | Adresa RADIUS serveru. Default adresa hosta ke kterému tato služba patří. Povinný údaj |
| `rad_eap_test_port` | Port . Default 1812. Povinný údaj. |
| `rad_eap_test_secret` | Tajemství. Povinný údaj. |
| `rad_eap_test_user` | Uživatel. Povinný údaj. |
| `rad_eap_test_pass` | Uživatelsé heslo. Povinný údaj. |
| `rad_eap_test_anonid` | anonymous_user@realm.tld |
| `rad_eap_test_timeout` | Časový limit na vyřízení požadavku. Default 5 sekund. |
| `rad_eap_test_method` | Metoda (WPA-EAP or IEEE8021X). Default WPA-EAP. |
| `rad_eap_test_eapmethod` | EAP metoda. Default PEAP. |
| `rad_eap_test_packets` | Vypiš všechny packety. True / False |
| `rad_eap_test_SSID` | SSID |
| `rad_eap_test_mac` | MAC adresa ve formátu xx:xx:xx:xx:xx:xx |
| `rad_eap_test_connectinfo` | Zapiš informaci o spojení (ukáže se v logu RADIUSu: coonect from). |
| `rad_eap_test_domain` | Omez na doménové jméno. FQDN se používá pro úplné porovnání serverového certifikátu. |
| `rad_eap_test_phase2` | Typ při Phase2 (PAP,CHAP,MSCHAPV2). |
| `rad_eap_test_subject` | Řeťezec k porovnání oproti subjektu certifikátu autentifikačního serveru. |
| `rad_eap_test_operator` | Operator-Name v doménovém formátu. |
| `rad_eap_test_nasaddress` | Explicitně spceifikuj NAS adresu. |
| `rad_eap_test_cui` | Vyžádej CUI. True / False |
| `rad_eap_test_calledstationid` | Pošli Called-Station-Id ve formátu MAC:SSID. |
| `rad_eap_test_printcert` | Pošli detaily o certifikátu RADIUS serveru. True / False |
| `rad_eap_test_showcui` | Ukaž CUI ebo Oprator-Name. True / False|
| `rad_eap_test_warncert` | Zkontroluj expiraci certifikátu. |
| `rad_eap_test_4` | Použij IPv4. Default. True / False|
| `rad_eap_test_6` | použij IPv6. True / False|

### check_cluster

Plugin je oficiální součástí projektu [monitoring-plugins](https://www.monitoring-plugins.org/doc/man/check_cluster.html)

Pro pochopení mechanismu doporučuji shlédnout [vlákno monioring-plugins](https://github.com/monitoring-plugins/monitoring-plugins/issues/1453).

```json
{   "attrs": {
    "check_command": "check_cluster",
    "vars": {
      "check_cluster_warning": "1",
      "check_cluster_critical": "2",
      "vars.check_cluster_objects:": [ "vpn1.ics.muni.cz!ping4", "vpn2.ics.muni.cz!ping4" ],
      "check_cluster_label:": "OpenVPN-server",
      "check_cluster_service:": "true"
    }
  }
}

```

Nastavitelné proměnné:

| Proměnná: | Vlastnost: |
| ------ | ------ |
| `check_cluster_host` | Zkontrluj status hosta. True / False |
| `check_cluster_service` | Zkontroluj status služby. True / False |
| `check_cluster_label` | Volitelný předřazený textový výstup (např.: \"Host cluster\") |
| `check_cluster_warning` | Udává rozsah hostů či služeb v clusteru které musí být v ne-OK stavu aby byl vrácen status WARNING. |
| `check_cluster_critical` | Udává rozsah hostů či služeb v clusteru které musí být v ne-OK stavu aby byl vrácen status CRITICAL. |
| `check_cluster_data` | Statusové kódy hostů či služeb v clusteru, oddělené čárkou. |
| `check_cluster_objects` | Definuje pole hostů/služeb v Icinze. Vyžaduje nastavení `check_cluster_service` nebo `check_cluster_host`. Používá se místo `check_cluster_data`. |

### check_dnssec_expiry
[Plugin](https://github.com/f-froehlich/check_dnssec_expiry) pro monitorování a validaci DNSSEC pro danou zónu za použití DNSSEC validujícího překladače.

```json
{   "attrs": {
    "check_command": "check_dnssec_expiry",
    "vars": {
      "dnssec_zone": "ics.muni.cz",
      "dnssec_warn": "30d",
      "dnssec_crit": "5d",
      "dnssec_resolver": "2001:718:1:1::2"
    }
  }
}
```

Nastavitelné proměnné:

| Proměnná: | Vlastnost: | Výchozí hodnota |
| ------ | ------ | ------ |
| `dnssec_zone` | Validovat zónu | muni.cz |
| `dnssec_warn` | Varuj když zbývá toliko dní do expirace | 20d |
| `dnssec_crit` | Kritický stav když zbývá toliko dní do expirace | 5d |
| `dnssec_resolver` | Použij resolver | [185.43.135.1](https://www.nic.cz/odvr/) |
| `dnssec_failing` | Nastav doménu která vždy selže (k ověření správné funkčnosti resolveru) | dnssec-failed.org |
| `dnssec_type` | Validuj DNS zýznam typu | SOA |
