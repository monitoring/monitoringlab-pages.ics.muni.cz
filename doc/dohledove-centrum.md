# Dohledové centrum

Zahlídání strojů, u kterých je žádané aby v případě problému provedlo dohledové centrum nějakou akci, je možné buď:

* nastavením uživatelské proměnné 'vars.dohled = Z*x*' u strojů určených k zahlídnání.
* použítím šablony "generic-host-dohled-Z*x*" nebo "generic-service-dohled-Z*x*" (*), která proměnnou nastaví při vytvoření stroje

Z*x* reprezentuje úroveň zahlídnání Z1 nebo Z2.

Dále je nezbytné napsat instrukce dohledovému centru -> jakou akci má za kterých okolností provést (pro to je možne použít interní [wiki](https://monitor.ics.muni.cz/wiki).Instrukce nemusí být nutně na interní wiki, ale musí být dostupné pro DC. Pro přístup do wiki napište na [icinga-adm@ics.muni.cz](mailto:icinga-adm@ics.muni.cz).

Tyto informace je možné uvést do poznámek pomocí objektu "[notes](https://icinga.com/docs/icinga2/latest/doc/09-object-types/#host)" nebo přes vlastní proměnné.

Zároveň do poznámek uveďte závažnost zahlídaných služeb dle zavedeného modelu. Nově bude Dohledové centrum hlídat pouze služby se závažností Z1 a Z2.

A na závěr konaktuje Dohledové centrum na adrese [bejcek@ics.muni.cz](mailto:bejcek@ics.muni.cz) s žádostí o zahlídání.

| Závažnost | Délka odezvy | Kdy | Jak | Kdo | Urgence RT |
| --------- | ------------ | --- | --- | --- | ---------- |
| Z1 | Okamžitě | 24/7 | Telefon | Správce služby | Každou hodinu |
| Z2 | V Hodinách |16/7 (06:00 - 22:00) | Telefon/E-mail | Správce služby | Následující pracovní den |

**(*) Mějte na paměti, že přes API není možné měnit šablony již zavedených hostů - v tom případě je lepší proměnnou přidat ručně nebo hosta smazat a znovu zavést.**
