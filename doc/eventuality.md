# Eventuality

Ke kontrole služby je možné také navázat eventualitu, která se má provést pokud dojde ke zmeně stavu. Eventualita je v tomto případě příkaz který se za určitých podmínek spustí a vykoná nějakou činnost.

Nejčastější využtí tato funkce najde v restartování spadnutých služeb, může ale provádět i jiné činnosti. Aktuálně je nadefinováno a podporováno spouštění příkazů přes NRPE. Je ale možne funkci rozšířit dle potřeb, třeba na Icinga agenta, WMI nebo cokoliv jiného.

Pro úplný výpis toho co **EventCommand** zvládne doporučuji [shlédnout dokumentaci](https://icinga.com/docs/icinga2/latest/doc/03-monitoring-basics/#event-commands)


## Příklad NRPE události

K atributům služby přidáme další atribut s názvem `event_command` specifikující kterou eventualitu chceme provést. Zatím je podporováno pouze NRPE. `event_comand` je podobně jako `check_command` dále kontrolován přes vlastní proměnné.

Příklad zahlídání procesu `radiator` a spuštění eventuality `restart_radiator`. Jakmile Icinga zjistí že proces není přítomen nebo je zombifikován (došlo ke změně stavu) vykoná svoji nadefinovanou činnost, v tomto příkladě restart procesu. Oba příkazy musí být nadefinovány na straně klienta:

NRPE.CFG:
```console
command[check_radiator]=unset LANG && /usr/lib/nagios/plugins/check_procs -C perl -a "radiusd -foreground -no_pid_file -config_file /etc/radiator/radius.cfg" -c 1:1 -s RsIDSs
command[restart_radiator]=sudo /usr/sbin/service radiator restart || echo "Radiator not reset. Something went wrong."
```

Definice na straně Icingy bude vypadat:

URI:
```
https://monitor.ics.muni.cz:5665/v1/objects/services/krakonos.ics.muni.cz\!check_radiator
```

DATA:
```json
{   "attrs": {
    "groups": [ 'skup1', 'skup2' ]
    "check_command": "nrpe",
    "event_command": "nrpe",
    "max_check_attempts": "3",
    "vars": {
      "nrpe_command": "check_radiator",
      "nrpe_event_command": "restart_radiator"
    }
  }
}
```


## Definice příkazů eventualit

### NRPE

| Proměnná: | Vlastnost: |
| --- | ---------------- |
| `nrpe_event_address` | Adresa NRPE klienta. Pokud není uvedeno, použije se adresa služby |
| `nrpe_event_port` | Port NRPE klienta. Default je `5666` |
| `nrpe_event_command` | Příkaz který má klient provést |
| `nrpe_event_no_ssl` | Bez TLS. Výchozí hodnota je `false` |
| `nrpe_event_timeout_unknown` | Vypršení časového limitu vrátí `UNKNOWN` | místo `CRITICAL`. Ve výchozím stavu vypnuto.
| `nrpe_event_timeout` | interval:stav = Čas vypršení spojení:Stav ukončení (exit code) v případě že dojde k vypršení časového limitu |
| `nrpe_event_arguments` | Argumenty které budou předány příkazu separované mezerou |
| `nrpe_event_ipv4` | Použij IPv4 |
| `nrpe_event_ipv6` | Použij IPv6 |
| `nrpe_event_version_2` | Požij packety pro NRPEv2 |
