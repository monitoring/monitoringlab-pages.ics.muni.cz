# O službě

Monitoring je služba pro zaměstnance univerzity, kteří potřebují monitorovat stav IT služeb, serverů nebo zařízení připojených k datové síti MU.

Nástroj se skládá z těchto částí:

 * Monitorovací nástroj Icinga2 na adrese [monitor.ics.muni.cz](https://monitor.ics.muni.cz)
 * Demo verze pak na adrese [icinga-demo](https://icinga-demo.ics.muni.cz/icingaweb2/)
 * Nástroj pro zobrazování metrik Grafana na adrese [grafana.ics.muni.cz](https://grafana.ics.muni.cz)
     * součástí instalace Grafany je [InfluxDB](https://www.influxdata.com/) - časosběrná databáze pro ukládání metrik
 * [DokuWiki](https://monitor.ics.muni.cz/wiki) pro instrukce Dohledovému centru a dokumentaci

Web je napojen na [Jednotné přihlášení MU](https://it.muni.cz/sluzby/jednotne-prihlaseni-na-muni). Pro přihlášení použijte UČO a primární heslo.

## Použití

Monitoring může používat každý zaměstnanec MU, nicméně před použitím je třeba seznámit se s [pravidly](../pravidla)

Nabízíme a udržujeme vlastní [Docker image](https://gitlab.ics.muni.cz/monitoring/icinga) pro ty kteří preferují mít vlastní instanci Icingy.

Poskytujeme konzultace pro vlastní konfiguraci, ne však konfiguraci samotnou!

 * [Nástroje pro konfiguraci](../nastroje)
 * [Doporučení a pravidla](../pravidla)
