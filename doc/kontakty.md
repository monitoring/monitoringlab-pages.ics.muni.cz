# Kontakty

* název RT fronty: `uvt-icinga-adm`
* e-mail: [icinga-adm@ics.muni.cz](mailto:icinga-adm@ics.muni.cz)

Sysadmin a vlastník služby:

* Marek Jaroš

Dohledové centrum:

* Tomáš Bejček ([bejcek@ics.muni.cz](mailto:bejcek@ics.muni.cz))
