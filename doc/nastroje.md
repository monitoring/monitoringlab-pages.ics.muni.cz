# Nástroje

Icingu je možné konfigurovat obecně jakýmkoliv nástrojem, který umožňuje pracovat s REST API.

## Ansible

Nástroj pro konfiguraci Icingy pomocí ansible je vzorový, není potřeba jej používat tak, jak je. Ke stažení v [Gitlab-u](https://gitlab.ics.muni.cz/monitoring/ansible), [podrobný popis](https://gitlab.ics.muni.cz/monitoring/ansible/blob/master/README.md)

## Puppet

Pokud používáte puppet na své infrastruktuře, můžete použít nástroj pro konfiguraci pomocí puppet-u. Nástroj je součást infrastruktury CERIT-SC, pokud máte zájem o použití, kontaktujte Lukáše Hejtmánka. [Popis použití](../nastroje/puppet). [Vzorové repo](https://gitlab.ics.muni.cz/cloud/puppet-icinga2).

## PowerShell

Powershell podporuje volání REST-ových API, je tedy možné použít PowerShell pro konfiguraci Icingy. Příklad použití v [repositáři](https://gitlab.ics.muni.cz/SCCM/powershell-icinga-module)

## Terraform

Další nástroj s kterým je možne Icingu přes API konfigurovat. Jednoduchý příklad použití je v [Gitlab-u](https://gitlab.ics.muni.cz/2780/tf-icinga-example)

## PHP

Konfigurace API pomoci PHP skriptu. Vzorový příklad použití najdete v [repositáři](https://gitlab.ics.muni.cz/433364/icinga2-config)

## Z příkazové řádky

Není samozřejmě nutné používat nástroje, Icingu lze konfigurovat přímo z příkazové řádky například pomocí nástroje cURL. Popis použití a vzorové příklady jsou k dispozici v [dokumentaci Icingy](https://icinga.com/docs/icinga2/latest/doc/12-icinga2-api/)
