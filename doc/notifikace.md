# Vytvoření notifikací

Prosté zahlídání stroje/služby nám pouze ukáže, v jakém stavu se daná služba nebo host nachází. Občas se ale hodí, aby o tomto stavu byl správce informován. K tomu slouží notifikace. Notifikace jsou objekty (stejně jako host nebo služba) provázané s jinými objekty.

Aktuální konfigurace Icingy umožňuje posílat notifikace pomocí e-mailu, SMS, Mattermost a MS Teams.

SMS notifikace jsou automaticky pozdrženy 3 minuty běhěm nichž se čeká na další zprávu, pokud přijde, bude spojena v jednu SMS. Pozdržení je zde z důvodu zamezení přetížení SMSkovače a také ke snížení upovídanosti v případě krátkého výpadku. Ostatní notifikace nejsou pozdrženy.

Napřed je ale potřeba mít jasno jaké notifikace, kdo a kdy má dostávat. Icinga umožňuje nastavit odesílání notifikací flexibilně dle potřéby správce služby. Například jen v pracovní dny, o víkendu nic. Aktuální konfigurace Icingy obsahuje tyto možnosti:

| Jméno notifikace:                                                               | Popis:                                                               |
| ----                                                                            | ----                                                                 |
| `mail-host-notification`                                                        | Posílá e-mail o každé změně hosta 24x7                               |
| `mail-service-notification`                                                     | Posílá e-mail o každé změně služby 24x7                              |
| `sms-host-notification`                                                         | Jako `mail-host-notification`, ale SMS.                              |
| `sms-service-notification`                                                      | Jako `mail-service-notification`, ale SMS                            |
| `sms-host-notification-workday`                                                 | SMS notifikace v době Po - Pá, 6:00 až 22:00                         |
| `sms-service-notification-workday`                                              | SMS notifikace v době Po - Pá, 6:00 až 22:00                         |
| `mail-host-notification-norenotify`                                             | Notifikaci pošle pouze jednou, vychází z `mail-host-notification`    |
| `mail-service-notification-norenotify`                                          | Notifikaci pošle pouze jednou, vychází z `mail-service-notification` |
| `mattermost_host`                                                               | Posílá notifikace hosta do kanálu Mattermost                         |
| `mattermost_service`                                                            | Posílá notifikace služby do kanálu Mattermost                        |
| `teams-host-notification`                                                       | Posílá notifikace hosta do kanálu v MS Teams                         |
| `teams-service-notification`                                                    | Posílá notifikace služby do kanálu v MS Teams                        |

Další možnosti je možné vytvořit. Zde doporučuji obrátit se na dokumentaci k [notifikacím](https://icinga.com/docs/icinga2/latest/doc/09-object-types/#notification) a [časový periodám](https://icinga.com/docs/icinga2/latest/doc/09-object-types/#timeperiod).

Zde uvedené jsou předdefinované šablony, samozřejmě je možné vytvořit si vlastní notifikace dle potřeby.

## Vytvoření uživatele

Dále je potřeba vytvořit uživatele, kteří budou dostávat notifikace. Aby takto vytvořený uživatel měl smysl, je potřeba mu nastavit atribut `e-mail` a pokud má dostávat SMS, tak i atribut `pager`. Seznam všech atributů je v [dokumentaci](https://icinga.com/docs/icinga2/latest/doc/09-object-types/#user).

Konkrétní volání API potom vypadá následovně:

DATA:

```json
{
  "attrs": {
    "display_name": "Cele Jmeno",
    "email": "uco@muni.cz",
    "pager": "+420549494038",
  }
}

```

URI:
```
https://monitor.ics.muni.cz:5665/v1/objects/users/uco@muni.cz
```

"uco" prosím nahraďte učem skutečného uživatele, stejně tak ostatní věci ;)

Takto vytvořený uživatel bude moci dostávat e-maily a SMS o stavu věcí. Každý uživatel si může nastavit jaké typy a stavy notifikací chce dostávat a samozřejmě kdy. Pokud mám více uživatelů, můžu je sjednotit do skupiny, ty se specifikují atributem `groups`.

## Vytvoření skupiny uživatelů

Uživatele je možné sdružovat do skupin, ty se vytváří takhle:

DATA:

```json
{
  "attrs": {
    "display_name": "popisne jmeno skupiny",
  }
}

```

URI:

```
https://monitor.ics.muni.cz:5665/v1/objects/usergroups/prefix-jmeno_skupiny
```

## Vytvoření notifikace pro uživatele/skupinu

DATA:

```json
{
  "attrs": {
    "interval": 300.0,
    "period": "",
    "users": [ "uco@muni.cz" ],
    "user_groups": [ "prefix-jmeno_skupiny" ],
    "command": "mail-host-notification",
    "service_name": "test-web",
    "host_name": "novy_stroj.ics.muni.cz"
  }
}
```

URI:

```
https://monitor.ics.muni.cz:5665/v1/objects/notifications/novy_stroj.ics.muni.cz!test-web!mail-notification
```

Podstatný je atribut `command` -- ten říká, jakým nástrojem se budou vytvářet notifikace. Seznam je v tabulce na začátku.
