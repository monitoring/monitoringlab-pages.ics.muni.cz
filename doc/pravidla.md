# Použití Monitoringu

## Varianty:

  * Správa monitorovaných strojů a služeb ve vlastní režii
  * Správa monitorovaných strojů a služeb správci služby
  * Satelit napojený na hlavní instanci
  * Vlastní instance

### Správa ve vlastní režii

Tento režim správy je určen každému, kdo si chce sám spravovat své stroje a služby. Konfigurace se provádí přes [API Icingy](https://icinga.com/docs/icinga2/latest/doc/12-icinga2-api/). Jak budou stroje a služby monitorovány, je po aplikaci níže uvedených a obecných pravidel zcela v režii správců monitorovaných strojů a služeb. Toto je doporučovaná forma použití monitoringu.

  * Monitorovat je možné pouze vlastní stroje
    * při potřebě informací o stavu stroje/služby, kterou provozuje někdo jiný, je třeba domluvit se na zahlídání
  * Při vytváření skupin v názvu použiji vhodný prefix.
    * kvůli předcházení kolizí ve jménech
    * např. cloud, cerit, diti, csirt, win...
    * mějte na paměti, že stále můžete použít `display_name`, kde si můžete zvolit vhodný název
  * Přes API je možné definovat i konfigurační soubory, je možné tedy využít potenciálu Icingy, avšak je třeba používat s rozvahou.
  * Případnou žádost o zahlídání strojů/služeb Dohledovým centrem poslat na adresu [bejcek@ics.muni.cz](mailto:bejcek@ics.muni.cz). Detaily v kapitole [Zahlídání strojů](../dohledove-centrum).

### Správa správci monitoringu

V tomto případě se o konfiguraci starají správci monitoringu. Správce serveru či služby zasílá své požadavky na monitoring správcům monitoringu a tito je realizují. V případě využití této formy je třeba omezení nad rámec obecných pravidel z důvodu zpřehlednění monitorovaných strojů a služeb:

  * Zasílání notifikací zůstane jednotné pro celý stroj -- není možné nastavit, aby služba X měla jinak nastavené notifikace než služba Y v rámci jednoho stroje.
  * Interval kontrol, změny stavu služby/stroje a notifikace o změně stavu je možné vybírat z předdefinovaných šablon
    * pokud neexistuje šablona, není možné například dostávat notifikace jen v první úterý v měsíci
  * Při vyšší složitosti nebo frekvenci změn budou tyto stroje/služby převedeny do vaší správy
  * Případnou žádost o zahlídání strojů/služeb Dohledovým centrem poslat na adresu [bejcek@ics.muni.cz](mailto:bejcek@ics.muni.cz). Detaily v kapitole [Zahlídání strojů](../dohledove-centrum).

### Satelit napojený na hlavní instanci

Satelit je v tomto připadě monitorovací jádro Icingy ve vlastní správě které komunikuje s masterem. Výsledky tedy budou zobrazeny na hlavním webu, ale konfigurace bude striktně ve vlastní režii. Pro satelit je doporučeno použít náš [Docker kontejner](https://gitlab.ics.muni.cz/monitoring/icinga), ale možné použít i klasickou instalaci z balíkovače OS.

  * Správa a konfigurace satelitu ve vlastní správě
  * Notifikace odesílá buď sám satelit, nebo preferovaně master ([hlavní instance Icingy](https://monitor.ics.muni.cz))
  * Možnost monitorovat oddělenou část sítě, kde by užití hlavní instance vyžadovalo mnoho pravidel firewallu
  * Nevýhodou je komplexita konfigurace společně s vlastní správou OS a satelitu

### Vlastní instance

Poskytuji [Docker obraz](https://gitlab.ics.muni.cz/monitoring/icinga) který umožuje spustit si vlastní Icingu.

  * Vše ve vlastní režii
  * Středně náročné na konfiguraci
  * Docker kontejner se snažím udržovat aktualizovaný
  * Vlastní instance nevytvářím, ani nepodporuji jinak než Docker kontejner samotný


## Obecné zásady použití

Toto jsou zásady pro monitoring, které jsou platné pro všechny. Otevřením možnosti konfigurace je třeba nastavit pravidla, aby konfigurace monitoringu byla udržitelná i do budoucna.

  * **Všichni uživatelé API mají administrátorská práva, proto je třeba změny v konfiguraci provádět uvážlivě**
  * Pro testování různých operací s API používejte testovací prostředí **icinga-demo.ics.muni.cz**
  * Každý stroj bude zaveden pod jménem odpovídajícím jeho FQDN a z popisu bude zřejmé, kdo je za něj zodpovědný
    * stroje bez DNS záznamu budou pojmenovány dle IP.
  * U stroje je důrazně doporučené definovat rodiče
    * například router, ke kterému je stroj připojený
  * Každý stroj je možné monitorovat pouze jednou
    * pokud má více skupin zájem na získávání notifikací o stavu, domluví se se správcem služby/stroje.
  * Stroje/služby/kontakty se mohou dávat pouze do vlastních skupin a do skupin globálních
    * pokud je potřeba vazba na cizí skupinu, je třeba domluvit se se správcem.
    * Globální skupiny mají v názvu *global*
  * Členství ve skupině se definuje u stroje/služby/kontaktu, ne ve skupině samotné.
    * změny v monitoringu jsou potom jednodušší
    * nevzniká překryv v konfiguraci


## Dobré vědět

* Pokud bude stroj/služba 14 dní v jiném než OK stavu bez jakékoliv komunikace ze strany správce stroje/služby (například nastavením `Acknowledge`, `Downtime` a podobně), bude odstraněna z monitoringu.
* Pokud bude stroj/služba 3 měsíce s nastaveným `Acknowledge` nebo `Downtime` v jiném než OK stavu bude odstraněna z monitoringu také.
* [icinga-demo](https://icinga-demo.ics.muni.cz) funguje jako střelnice a stroje mohou být občas promazány. Obsahuje také novější verze interních komponent než jsou nasazeny na produkci.
* Správa uživatelů pro webové rozhrnaní je možná i přes [Perun](https://perun.aai.muni.cz/). Stačí vytvořit skupinu a nahlásit ji jako zdroj uživatelů.
