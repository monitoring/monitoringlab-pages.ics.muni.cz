# Příklady konfigurace

## Ansible:
```yaml
- uri:
    url: "http://host/api"
    user: "username"
    password: "password"
    method: POST
    return_content: yes
    headers:
      Accept: "application/json"
      Content-Type: "application/json"
    body_format: "json"
    body: {"configure":".*", "write":".*", "read":".*"}
```

## Powershell:
```powershell
$url = "https://monitor.ics.muni.cz:5665/v1"
$secpasswd = ConvertTo-SecureString "zde_je_heslo" -AsPlainText -Force
$mycreds = New-Object System.Management.Automation.PSCredential ("uzivatel_API", $secpasswd)
$stuff = Invoke-RestMethod -Uri $url -Method Get -Credential $mycreds

```

## cURL:
```shell
$ curl -i -u username:password -H "content-type:application/json" -XPOST http://host/api -d '{"configure":".*","write":".*","read":".*"}'
```

## Označení strojů a služeb pro zahlídání Dohledovým Centrem
```curl -s -u 'user_name:password' -X POST -H 'Accept: application/json' -d ''{"attrs": { "check_command": "hostalive", "vars": {"dohled": Zx}}'https://name-user:xxxx:xxxx@monitor.ics.muni.cz:5665/v1/objects/hosts/hostname.domain.cz'```

## Puppet

### Příklad vytvoření hosta (Puppet)

```
icinga2::host{ $facts['fqdn']:
    check_command        => "hostalive",
    address              => $facts['ipaddress'],
    groups               => ["skupina-cerit"],
    templates            => ["generic-host"],
    enable_notifications => true,
}
```

## Příklad vytvoření služby (Puppet)

```
 icinga2::service { 'check_ssh_via_nrpe':
      check_command => "nrpe",
      vars          => { "nrpe_port" => 5669, "nrpe_command" => 'check_ssh' },
}
```
