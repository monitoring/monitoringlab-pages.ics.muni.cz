# Vytvoření hosta

Vlastnosti objektu, tedy například hosta nebo služby, se dají ovlivnit pomocí atributu `vars`. Například `body`:

```json
{ "templates": [ "generic-host" ],
  "attrs": {
    "address": "192.168.1.1",
    "check_command": "hostalive",
    "vars.os" : "Linux",
    "vars.nrpe_port" : "5669"
  }
}

```

Vytvoří hosta s IP adresou 192.168.1.1 a `vars` má dvě: `os` a `nrpe_port`. Zároveň se vloží atributy definované v šablonách. Ty jsou vybrané atributem `templates` v předchozím příkladu. Šablonu lze použít skoro všude, takže u hosta, služby, skupiny...

## Vytvoření skupin

Pro začátek je potřeba vytvořit potřebné skupiny, do kterých bude host patřit. 

Konfigurace pro takovou skupinu vypadá takhle:

```
object HostGroup "linux-servers" {
  display_name = "Linux Servers"

  assign where host.vars.os == "Linux"
}
```

Tento zápis říká, že tento objekt se má aplikovat za splnění podmínky. V tomto případě když je `host.vars.os` nastaveno na "Linux". Tedy například host vytvořený předchozím kódem. Konstrukce `assign where ...` není povinná, není nutné ji použít. V tom případě bude potřeba specifikovat členství ve skupinách při definici hosta/service. Problém je v tom, že objekty s klauzulí `assign where` nelze vytvářet přes API. V případě, že chcete vytvořit skupinu s `assign where`, je potřeba kontaktovat administrátory a požádat o vytvoření skupiny ručně v konfiguraci. Toto platí pro Host-, Service-, User- groupy. Více v [dokumentaci icingy](https://icinga.com/docs/icinga2/latest/doc/17-language-reference/#group-assign).

Vytváření skupiny přes API potom vypadá následovně:

### HostGroup

DATA:

```json
{
  "attrs": {
    "display_name": "Prefix: Jmeno skupiny"
  }
}
```

URI:

```
https://monitor.ics.muni.cz:5665/v1/objects/hostgroups/prefix-jmeno_skupiny
```

### ServiceGroup

Pravidla pro vytváření Servicegroups jsou identická, jako pro HostGroups, data jsou též identická, liší se pouze URI:

```
https://monitor.ics.muni.cz:5665/v1/objects/servicegroups/prefix-jmeno_skupiny
```

## Host

Nyní můžeme začít monitorovat nějaký stroj. K tomu potřebujeme:

1. hostname -- celé doménové jméno -- například `monitor.ics.muni.cz`. `monitor` nestačí, protože toto jméno nedostatečně identifikuje stroj. Pokud stroj nemá záznam v DNS, zvažte jeho zřízení, pokud to má smysl.
1. IP adresy -- IPv4 i IPv6, stačí alespoň jedna...
1. způsob, jak ověříte, že server je živý -- běžný je ping.
1. a samozřejmě skupiny, do kterých má host patřit.

Dále doporučujeme nastavit GPS souřadnice, kde se host nachází. Potom budete moci procházet mapu a lokality. Potřebné se nastaví pomocí custom var:

```
vars.geolocation = "<latitude>,<longitude>"
```

K dispozici jsou tyto zkratky pro lokace:

| Místo: | Název: |
|--------|--------|
| Sál A510 na FI | srv_a510 |
| Serverovna Šumavská (Gotex) | srv_gotex |
| Serverovna Botanická, starý sál | srv_uvt_stary |
| Serverovna Botanická (core) | srv_ics |
| Serverovna Komenského 2 | srv_cps |
| Serverovna RMU Žerotínovo nám. 9 | srv_rect |
| Serverovna ESF Lipová  | srv_econ |
| Serverovna PedF, Poříčí 7  | bb_ped |
| Serverovna FF | bb_phil |
| Serverovna SKM Komárov, Žúrků  | bb_koma |
| Serverovna PrF, Veveří | bb_law |
| Serverovna UKB LK SZ | bb_ukb_sever |
| Serverovna UKB LK JZ | bb_ukb_jih |
| Serverovna UKB A35 | bb_ukb_a35 |
| Serverovna Vinařská | bb_vin |
| Serverovna FSS Joštova | bb_fss |
| Serverovna PřF Kotlářská | bb_sci |
| SLP rozvodna Družba | bb_druz |
| SLP rozvodna nám. Míru | bb_miru |
| SLP rozvodna SKM Klácelova | bb_klac |
| SLP rozvodna FF Grohova | bb_groh |
| SLP rozvodna Kino Scala | bb_scala |
| SLP rozvodna SKM Tvrdého | bb_tvrd |
| SLP rozvodna SKM Sladkého | bb_slad |
| SLP rozvodna Ústavu fyziky Země | bb_ufz |
| UC Telč | bb_telc |


Takže místo `vars.geolocation` můžete použít `vars.muni_loc`, který se vyhodnocuje přes šablonu `geo-host`, například:

```
vars.muni_loc = GeoLocation["srv_ics"]
```

Pomocí `vars` můžeme nastavit další potřebné atributy. Tyto je ale možné v budoucnu měnit modifikací hosta.

Konkrétní příklad může potom vypadat takto:

URI:

```
https://monitor.ics.muni.cz:5665/v1/objects/hosts/novy_stroj.ics.muni.cz
```

Za `novy_stroj.ics.muni.cz` dejte samozřejmě FQDN.

DATA:

```json
{
  "attrs": {
    "address": "192.168.1.1",
    "display_name": "Nový webový server",
    "check_command": "hostalive",
    "groups": [ "tymova_skupina", "ostatni_skupiny" ],
    "vars.os": "Linux",
    "vars.muni_loc": "srv_a510"
  }
}
```
Toto je minimální příklad, povinné je pouze `check_command`. Atribut `groups` je povinný z hlediska identifikace stroje, `vars.muni_loc` doporučený kvůli lokaci stroje. Doporučuji přidat následující atributy:

| Atribut: | Popis |
|----------|-------|
| notes | krátké poznámky k hostovi |
| notes\_url | adresa, kde se nachází dokumentace ke stroji, například info pro Dohledové centrum |
| event\_command | Příkaz, který je vyvolán při změně stavu -- vhodné pro navázanou automatizaci |

Výčet všeho, co lze u hosta nastavit je v [dokumentaci](https://icinga.com/docs/icinga2/latest/doc/09-object-types/#host).


## Služba

Nyní potřebujeme vědět co vlastně potřebujeme monitorovat. Monitorované věci se dají rozdělit do dvou skupin - hlídané „z venku“ a „zevnitř“. Hlídané služby z venku jsou typu jestli běží HTTP server, tedy věci viditelné z venku po síti. Věci hlídané zevnitř jsou jako volné místo na disku. Tyto je možné hlídat přes NRPE.

Kontrola stavu se provádí pouštěním programů, které zjistí dle nastavených parametrů stav služby. Jejich seznam, popis a parametry jsou k dispozici v [dokumentaci Icingy](https://icinga.com/docs/icinga2/latest/doc/10-icinga-template-library/).

Na serveru běží například HTTPS a potřebujeme hlídat volné místo na disku.

### HTTPS

Stav služby zjišťujeme tak, že se serveru doptáme, co nám po HTTPS pošle. Pokud odpověď odpovídá požadavkům, je služba v pořádku. Přes webové rozhraní lze najít `http` command. Tento dělá to, co potřebujeme.

URI:

```
https://monitor.ics.muni.cz:5665/v1/objects/services/novy_stroj.ics.muni.cz!test-web
```

`novy_stroj.ics.muni.cz` je jméno stroje použité v předchozím případě, `test-web` je jméno služby. To by mělo být vhodně zvolené.

DATA:

```json
{
  "templates": [ "generic-service" ],
  "attrs": {
    "check_command": "http",
    "check_interval": 60,
    "retry_interval": 30,
    "vars": {
      "http_ssl": "true",
      "http_port": "443"
  }
}
```

Takto vytvořená kontrola bude každou minutu (`check_interval`) kontrolovat dostupnost webu přes SSL na portu 443 (`http_ssl`, `http_port`). Commandem `http` lze kontrolovat i platnost certifikátu:

URI:

```
https://monitor.ics.muni.cz:5665/v1/objects/services/novy_stroj.ics.muni.cz!test-ssl-cert
```

DATA:

```json
{
  "attrs": {
    "check_command": "http",
    "check_interval": 86400,
    "retry_interval": 21600,
    "vars": {
      "http_ssl": "true",
      "http_certificate": "14"
  }
}
```
V tomto případě bude Icinga jednou denně kontrolovat, jestli certifikát má platnost delší než 14 dní.

### Volné místo na disku

Volné místo na disku nemohu zjistit přímo, zde je potřeba mít agenta. Jedna z možností, jak toto realizovat, je NRPE server, který na monitorovaném stroji pouští testy, které lokálně zjišťují stav stroje. Pouštění se děje na vyžádání ze strany Icingy. Toto chování je zcela identické jako v případě Nagios-u, takže pokud máte něco zahlídané v něm, toto není pro vás jistě nic nového.

Na straně monitorovaného serveru je pro náš příklad potřéba nakonfigurovat NRPE server. Do konfigurace se vloží řádek, jako je tento:

```
command[check_disk_www]=/usr/lib/nagios/plugins/check_disk -w 20% -c 10% -p /var/www/
```

Tento řádek říká, že se má kontrolovat volné místo na disku pomocí pluginu `check_disk`, pokud bude méně než 20% volného místa, je stav WARNING, méně než 10% CRITICAL. Kontroluje se dostupné místo v adresáři `/var/www/`. Tento příkaz se pak jmenuje `check_disk_www`. Toto jméno se použije při definici kontroly v Icinze.

Konfigurace pro Icingu bude tedy následujíci:

URI:

```
https://monitor.ics.muni.cz:5665/v1/objects/services/novy_stroj.ics.muni.cz!test-disk_www
```

DATA:

```json
{
  "attrs": {
    "check_command": "nrpe",
    "check_interval": 600,
    "vars": {
      "nrpe_command": "check_disk_www"
  }
}
```

Tímto máme zahlídaný stroj a služby na něm. Pro správnou funkci monitorování je třeba ještě doplnit další funkcionalitu. Prvně je potřeba [nastavit si jak a kdy budou odesílány notifikace o změně stavu služby/hosta](../notifikace).


## Grafy

Pro vytváření grafů se používá [Grafana](https://grafana.ics.muni.cz). Grafy jsou generovány dynamicky a zobrazují se automaticky pro majoritu dostupných kontrol.  
Pro lepší zobrazení grafů jsou ještě připraveny následující šablony. Pro jejich užití stačí použít jako jméno služby:

```
check_ping
check_ping6
check_procs
check_swap
check_disk
check_users
check_load
check_memory
```

Tedy pokud je třeba zobrazit vytížení v grafu je třeba jako jméno služby zvolit "check_load".

`https://monitor.ics.muni.cz:5665/v1/objects/services/novy_stroj.ics.muni.cz!check_load`

Pokud dynamicky generované či stávájící šablony nevyhovují zkuste si v [Grafaně](https://grafana.ics.muni.cz) vytvořit vlastní. Můžu ji potom namapovat přímo na stroj či službu.

Pokud dynamicky generovaný graf u některé kontroly (definované v `check_command`) nefunguje [napište mi](../kontakty) a pokusím se jej nakonfigurovat.
