# Závisloti

Slouží k definování závislostí mezi hosty/službami. Umožňuje to například zjednodušení notifikací -- pokud vypadne SQL server, tak není třeba hlásit nedostupnost všech webů, které tento SQL server využívají.

Podrobnější dokumentace o použití je [k dispozici](https://icinga.com/docs/icinga2/latest/doc/03-monitoring-basics/#dependencies) a [popis objektu Dependency](https://icinga.com/docs/icinga2/latest/doc/09-object-types/#dependency)

## Vytvoření závislostí mezi hosty

Slouží například k definování nadřazeného síťového prvku. Při jeho nedostupnosti je jasné, že servery za ním jsou nedostupné také. Nutná podmínka je, že oba dva hosti musí být v Icinze monitorovaní. Není tak možné vytvořit závislost na prvku, který není monitorován.

Pokud mám hosta child.muni.cz a chci vytvořit závislost na parent.muni.cz:

URI:

```
https://monitor.ics.muni.cz:5665//v1/objects/dependencies/child.muni.cz!parent.muni.cz
```

DATA:
```
{
    "attrs": {
        "parent_host_name": "parent.muni.cz",
        "child_host_name": "child.muni.cz", 
    }
}
```


## Vytvoření závislosti mezi službami

Vytvoří specifičtější závislost než mezi hosty. Závislost vytváří mezi konkrétními službami na hostech. Opět, hosti musí být definovaní, stejně tak i služby. Příkladem může být web, který ke svému běhu potřebuje funkční databázi.

Pokud vezmeme stroje z předchozího příkladu, tak na stroji child.muni.cz běží webový server, který je hlídaný přes service HTTPS a na stroji parent.muni.cz je zase zahlídaná service MYSQL. Volání API potom bude vypadat následovně:

URI:
```
https://monitor.ics.muni.cz:5665//v1/objects/dependencies/child.muni.cz!HTTPS!parent.muni.cz
```

DATA:
```
{
    "attrs": {
        "parent_host_name": "parent.muni.cz",
        "parent_service_name": "MYSQL",
        "child_host_name": "child.muni.cz", 
        "child_service_name": "HTTPS"
    }
}
```
